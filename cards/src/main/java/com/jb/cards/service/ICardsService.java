package com.jb.cards.service;

import com.jb.cards.dto.CardsDto;

public interface ICardsService {
    public void createCard(String mobileNumber);
    CardsDto fetchCard(String mobileNumber);
    boolean updateCard(CardsDto cardsDto);
    boolean deleteCard(String mobileNumber);
}
